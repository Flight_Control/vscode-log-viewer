#!/bin/bash
set -euxo pipefail

# this export is required
export DISPLAY=:99.0

if [ -z "$(pidof /usr/bin/Xvfb)" ]
then
    Xvfb -ac $DISPLAY &
fi

# running this inside Dockerfile doesn't seem to work
corepack prepare pnpm@latest --activate

cd /mnt/src/

pnpm run clean
pnpm install
pnpm add --global vsce
pnpm run package
pnpm run build
pnpm run lint

pnpm run test "${1:-""}"
