#!/usr/bin/env bash

set -euxo pipefail

if [ -z "$VS_MARKETPLACE_ACCESS_TOKEN" ]; then
    echo "VS_MARKETPLACE_ACCESS_TOKEN not set"
    exit 1
fi

if [ -z "$OPEN_VSX_ACCESS_TOKEN" ]; then
    echo "OPEN_VSX_ACCESS_TOKEN not set"
    exit 1
fi

pushd "$(dirname "$0")"

rm -f ./*.vsix

pnpm install

pnpm test minimum
pnpm test stable
pnpm test insiders

vsce package --no-dependencies

exts=(./*.vsix)
if [ ${#exts[@]} -eq 0 ]; then
    echo "extension not found"
    exit 1
else
    ext=${exts[0]}
fi

vsce publish --packagePath "$ext" --pat "$VS_MARKETPLACE_ACCESS_TOKEN"
ovsx publish "$ext" --pat "$OPEN_VSX_ACCESS_TOKEN"