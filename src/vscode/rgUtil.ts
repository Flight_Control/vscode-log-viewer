import * as vscode from "vscode";
import * as path from "path";
import { lsPattern } from "../common/fsUtil";
import { FsWalker, FsWalkerSubscription, FsWalkerOptions } from "../common/fsWalker";
import { rgInfos } from "../common/rgUtil";
import { toPathMatcher } from "../common/mmUtil";

let _rgPath: string | undefined;
export async function checkRgPath(): Promise<boolean> {
    let res = false;
    const pathMatcher = toPathMatcher(path.join(vscode.env.appRoot, "**", "rg?(.exe)"));
    await lsPattern(pathMatcher, f => {
        const name = path.basename(f.fullPath);
        if (name === "rg" || name === "rg.exe") {
            _rgPath = f.fullPath;
            res = true;
        }
    });
    return res;
}

export function rgPathFound(): boolean {
    return typeof _rgPath === "string";
}

export class RgWalker implements FsWalker {
    public readonly rgPath: string;
    constructor(private readonly opts: FsWalkerOptions) {
        if (!_rgPath) {
            throw new Error("Couldn't find rg");
        } else {
            this.rgPath = _rgPath;
        }
    }
    public walk(sub: FsWalkerSubscription): Promise<void> {
        return rgInfos(this.rgPath, this.opts, sub);
    }
}
