import * as vscode from "vscode";
import { LogWatchProvider } from "./logProvider";
import { toLogUri } from "./logUri";
import { WatchEntry } from "../common/config";
import { ConfigService } from "./config";
import { Logger } from "../common/logger";

export const openLogResourceCmd = "logviewer.openLogResource";
const unwatchCmd = "logviewer.unwatchLogResource";
const unwatchAllCmd = "logviewer.unwatchAll";

interface GroupItem {
    readonly kind: "group";
    readonly parent: GroupItem | undefined;
    readonly name: string;
    readonly items: Item[];
}

interface WatchItem {
    readonly kind: "watch";
    readonly parent: GroupItem | undefined;
    readonly title?: string;
    readonly pattern: string | string[];
    readonly uri: vscode.Uri;
}

type Item = GroupItem | WatchItem;

function toItem(entry: WatchEntry, parent: GroupItem | undefined): Item {
    if (entry.kind === "group") {
        const items: Item[] = [];
        const groupItem: GroupItem = {
            kind: "group",
            parent,
            name: entry.groupName,
            items,
        };
        for (const x of entry.watches) {
            items.push(toItem(x, groupItem));
        }
        return groupItem;
    } else {
        return {
            kind: "watch",
            parent,
            pattern: entry.pattern,
            title: entry.title,
            uri: toLogUri(entry),
        };
    }
}

class LogExplorer implements vscode.TreeDataProvider<Item>, vscode.Disposable {
    private readonly disposable: vscode.Disposable;

    public static readonly ViewId: string = "logExplorer";

    private readonly _onDidChange: vscode.EventEmitter<undefined>;

    constructor(private readonly logProvider: LogWatchProvider, private readonly configSvc: ConfigService) {
        this._onDidChange = new vscode.EventEmitter();

        this.disposable = vscode.Disposable.from(
            this._onDidChange,
            configSvc.onChange(() => {
                this._onDidChange.fire(undefined);
            }),
        );
    }

    public get onDidChangeTreeData() {
        return this._onDidChange.event;
    }

    public reload() {
        this._onDidChange.fire(undefined);
    }

    public getTreeItem(element: Item) {
        if (element.kind === "group") {
            return new vscode.TreeItem(element.name, vscode.TreeItemCollapsibleState.Expanded);
        } else {
            const watching = this.logProvider.has(element.uri);
            let name: string;
            if (element.title) {
                name = element.title;
            } else if (Array.isArray(element.pattern)) {
                name = element.pattern.join(",");
            } else {
                name = element.pattern;
            }
            const item = new vscode.TreeItem(name);
            if (watching) {
                item.iconPath = new vscode.ThemeIcon("eye");
                item.contextValue = "watching";
            } else {
                item.iconPath = undefined; //vscode.ThemeIcon.File;
                item.contextValue = undefined;
            }
            item.command = {
                command: openLogResourceCmd,
                arguments: [element.uri],
                title: name,
                tooltip: name,
            };
            return item;
        }
    }

    public getParent(element: Item): GroupItem | undefined {
        return element.parent;
    }

    public getChildren(element?: Item): Item[] | undefined {
        if (element === undefined) {
            // root
            return this.configSvc.getWatches().map(x => toItem(x, undefined));
        } else if (element.kind === "group") {
            return element.items;
        }
        return;
    }

    public dispose() {
        this.disposable.dispose();
    }
}

export interface LogExplorerAndTreeView {
    logExplorer: LogExplorer;
    treeView: vscode.TreeView<Item>;
}

export function registerLogExplorer(
    logProvider: LogWatchProvider,
    subs: vscode.Disposable[],
    configSvc: ConfigService,
    logger: Logger,
): void {
    const logExplorer = new LogExplorer(logProvider, configSvc);
    subs.push(logExplorer);

    const treeView = vscode.window.createTreeView(LogExplorer.ViewId, {
        treeDataProvider: logExplorer,
    });
    subs.push(treeView);

    if (global.__logViewer$testing) {
        global.__logViewer$logExplorerAndTreeView = {
            logExplorer,
            treeView,
        };
    }

    const descUnknown = (x: unknown): string => {
        if (typeof x === "string") {
            return x;
        }
        const s = String(x);
        if (s !== "[object Object]") {
            return s;
        }
        try {
            return JSON.stringify(s);
        } catch {
            return s;
        }
    };

    subs.push(
        vscode.commands.registerCommand(openLogResourceCmd, async (logUri: unknown) => {
            let uri: vscode.Uri;
            if (logUri instanceof vscode.Uri) {
                uri = logUri;
            } else if (typeof logUri === "string") {
                uri = vscode.Uri.parse(logUri);
            } else {
                logger.error(`unexpected argument type for "${openLogResourceCmd}": ${descUnknown(logUri)}`);
                return;
            }
            await logProvider.watchUri(uri);
            logExplorer.reload();
            const doc = await vscode.workspace.openTextDocument(uri);
            await vscode.window.showTextDocument(doc, { preview: false });
        }),
    );

    subs.push(
        vscode.commands.registerCommand(unwatchCmd, (x: unknown) => {
            let uri: vscode.Uri;
            if (typeof x === "string") {
                uri = vscode.Uri.parse(x);
            } else if ((x as { uri: vscode.Uri }).uri instanceof vscode.Uri) {
                uri = (x as { uri: vscode.Uri }).uri;
            } else {
                logger.error(`unexpected argument type for "${openLogResourceCmd}": ${descUnknown(x)}`);
                return;
            }
            logProvider.unWatch(uri);
            logExplorer.reload();
        }),
    );

    subs.push(
        vscode.commands.registerCommand(unwatchAllCmd, () => {
            logProvider.unWatchAll();
            logExplorer.reload();
        }),
    );
}
