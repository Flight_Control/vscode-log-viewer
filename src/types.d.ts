// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { LogExplorerAndTreeView } from "./vscode/logExplorer";

declare global {
    namespace NodeJS {
        interface Global {
            __logViewer$testing: boolean;
            __logViewer$logExplorerAndTreeView: LogExplorerAndTreeView;
        }
    }
}
