// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";

import { checkRgPath } from "./vscode/rgUtil";
import { setDevEnv, isDevEnv } from "./common/util";
import { ConfigService } from "./vscode/config";
import { registerLogger } from "./vscode/logger";
import { registerLogWatchProvider } from "./vscode/logProvider";
import { registerStatusBarItems } from "./vscode/statusBarItems";
import { registerLogExplorer } from "./vscode/logExplorer";
import { registerInstance } from "./common/container";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext): void {
    setDevEnv(context.extensionMode === vscode.ExtensionMode.Development);

    const subs = context.subscriptions;

    const configSvc = new ConfigService();
    subs.push(configSvc);
    registerInstance("config", configSvc);

    const logger = registerLogger(subs, configSvc);
    const logProvider = registerLogWatchProvider(subs, configSvc, logger);
    registerStatusBarItems(logProvider, subs, configSvc);
    registerLogExplorer(logProvider, subs, configSvc, logger);

    if (isDevEnv()) {
        checkRgPath()
            .then(found => {
                if (!found) {
                    void vscode.window.showWarningMessage("rg could not be found");
                } else {
                    void vscode.window.showInformationMessage("rg found");
                }
            })
            .catch(e => {
                logger.error(e);
            });
    }
}

// this method is called when your extension is deactivated
export function deactivate(): void {}
