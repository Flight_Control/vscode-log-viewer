import assert = require("assert");
import * as logUri from "../src/vscode/logUri";
import { checkRgPath } from "../src/vscode/rgUtil";
import * as path from "path";
import { FsWalkerFun, getNodeModulesDir, myWalkerFun } from "./vscodefree/testUtil";
import { rgWalkerFun } from "./testUtil";
import { patternDescription } from "../src/common/util";

suite("logUri", () => {
    test(`should not fail`, () => {
        const titleTestCases = ["/foo/bar", "α😀😂😉ω", "C:\\ñ.loga"];
        const patternTestCases = ["/foo/bar/**/*.log", "C:\\user\\s & s*.log"];

        for (const title of titleTestCases) {
            for (const pattern of patternTestCases) {
                const expected: logUri.WatchForUri = {
                    id: Math.round(Math.random() * 1000),
                    pattern: pattern,
                    title: title,
                    workspaceName: `w${title}${pattern}`,
                };
                const uri = logUri.toLogUri(expected);
                const actual = logUri.fromLogUri(uri);

                assert.deepStrictEqual(actual, expected);
            }
        }
    });

    test(`picks right extension`, () => {
        const testCases: Array<{ input: string; expected: string }> = [
            { input: "/foo/bar/**/*.log", expected: ".log" },
            { input: "/foo/bar{1,3}/**/*.log*", expected: ".log" },
            { input: "/foo/bar/**/*asd*txt", expected: ".log" },
            { input: "/foo/bar/**/*asd*txt.", expected: ".log" },
            { input: "/foo/(bar|baz)/**/*asd*.txt", expected: ".txt" },
            { input: "/foo/bar/**/*a[a]sd*.xml", expected: ".xml" },
        ];

        function verifyExtension(wc: logUri.WatchForUri, expectedExtension: string) {
            const uri = logUri.toLogUri(wc);
            assert(
                uri.path.endsWith(expectedExtension),
                `Pattern: ${patternDescription(
                    wc.pattern,
                )}, "${uri.toString()}" didn't end with "${expectedExtension}"`,
            );
        }

        for (const tc of testCases) {
            verifyExtension(
                {
                    id: 134,
                    pattern: tc.input,
                    workspaceName: undefined,
                },
                tc.expected,
            );
            verifyExtension(
                {
                    id: 556,
                    pattern: tc.input,
                    title: "some title",
                    workspaceName: undefined,
                },
                tc.expected,
            );
        }
    });
});

suite("FsWalker", () => {
    async function compareWalkers(actualFun: FsWalkerFun, expectedFun: FsWalkerFun) {
        const patterns = ["**/*.d.ts", "**/*.json", "**/*.xlsx"];

        async function collect(pattern: string, f: FsWalkerFun): Promise<string[]> {
            const walker = f(pattern);
            const acc: string[] = [];
            await walker.walk({
                onError: err => assert.fail(err.toString()),
                onFile: fi => acc.push(fi.fullPath),
            });
            return acc.sort();
        }

        const nodeModulesDir = getNodeModulesDir();

        for (const relPattern of patterns) {
            const fullPattern = path.join(nodeModulesDir, relPattern);
            const actual = await collect(fullPattern, actualFun);
            const expected = await collect(fullPattern, expectedFun);
            assert.deepStrictEqual(expected, actual);
        }
    }

    test("compare MyWalter and RgWalker", async function () {
        this.timeout(5000);
        await checkRgPath();
        await compareWalkers(myWalkerFun, rgWalkerFun);
    });
});
