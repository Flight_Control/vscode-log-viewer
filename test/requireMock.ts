import Module = require("module");

type Obj = Record<string, unknown>;

const originalRequire = Module.prototype.require;
Module.prototype.require = function (this: NodeJS.Require, ...args) {
    const name = args[0];
    if (name === "vscode") {
        if (_withVscode) {
            const orig = originalRequire.apply(this, args) as Obj;
            return getOrAddProxy(name, orig);
        } else {
            const mock = getOrAddMock(name);
            return mock;
        }
    } else if (name === "path") {
        const orig = originalRequire.apply(this, args) as Obj;
        return getOrAddProxy(name, orig);
    } else {
        return originalRequire.apply(this, args) as Obj;
    }
} as NodeJS.Require;

let _withVscode = false;
export function withVscode(): void {
    _withVscode = true;
}

const _proxies: { [name: string]: Obj } = {};
function getOrAddProxy(name: string, orig: Obj): Obj {
    let p = _proxies[name];
    if (!p) {
        const ph: ProxyHandler<Obj> = {
            get(target, prop_) {
                const prop = prop_ as string;
                const mock = _mocks[name];
                if (mock && mock[prop]) {
                    return mock[prop];
                } else {
                    return target[prop];
                }
            },
        };
        p = new Proxy(orig, ph);
        _proxies[name] = p;
    }
    return p;
}

const _mocks: { [name: string]: Obj } = {};
function getOrAddMock(name: string): Obj {
    let mock = _mocks[name];
    if (typeof mock !== "object") {
        mock = {};
        _mocks[name] = mock;
    }
    return mock;
}
export function setMock(moduleName: string, mock: Obj): void {
    const curMock = getOrAddMock(moduleName);
    Object.assign(curMock, mock);
}

export function clearMocks(): void {
    for (const name of Object.keys(_mocks)) {
        const mock = _mocks[name];
        if (typeof mock === "object") {
            for (const key of Object.keys(mock)) {
                delete mock[key];
            }
        }
    }
}
