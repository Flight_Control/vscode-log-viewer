import { FsWalker } from "../../src/common/fsWalker";
import { toPathMatcher } from "../../src/common/mmUtil";
import { SinglePathMatcherWalker } from "../../src/common/fsUtil";
import * as path from "path";
import { ConfigTypeMap } from "../../src/common/config";
import { setMock } from "../requireMock";
import { registerInstance } from "../../src/common/container";

export type FsWalkerFun = (pattern: string) => FsWalker;

export const myWalkerFun: FsWalkerFun = pattern => {
    const pm = toPathMatcher(pattern);
    return new SinglePathMatcherWalker(pm);
};

export function getNodeModulesDir(): string {
    return path.resolve(__dirname, "..", "..", "..", "node_modules");
}

let _config: Partial<ConfigTypeMap> = {};

export function testsInit(): void {
    registerInstance("config", {
        get(k) {
            return _config[k];
        },
        getEffectiveWatchOptions(_id: number) {
            throw new Error("Not implemented");
        },
        getWatches() {
            return [];
        },
        onChange() {
            // TODO impl this for tests?
            return {
                dispose() {},
            };
        },
    });
}

export function testSetConfiguration(config: Partial<ConfigTypeMap>): void {
    _config = config;
}

export function testSetPathSep(sep: typeof path.sep): void {
    setMock("path", {
        get sep() {
            return sep;
        },
    });
}
