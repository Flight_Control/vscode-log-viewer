import assert = require("assert");
import * as vscode from "vscode";
import { delay } from "../src/common/util";
import { promises as fs, PathLike, constants as fsConstants } from "fs";
import * as path from "path";
import { encode } from "iconv-lite";
import * as inspector from "inspector";

const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 \t" +
    "~€‚ƒ„…†‡ˆ‰Š‹ŒŽ‘’“”•–—˜™š›œžŸ¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";

function randomString() {
    const len = 20 + Math.round(Math.random() * 100);
    const charCodes = new Array<number>(len);
    for (let i = 0; i < charCodes.length; i++) {
        charCodes[i] = characters.charCodeAt(Math.floor(Math.random() * characters.length));
    }
    return String.fromCharCode(...charCodes);
}

interface FilesAndDirectories {
    files: string[];
    directories: string[];
}

function randomFileName() {
    const str = Math.random().toString(16);
    if (str.length === 1) {
        // in case of 0 or 1
        return str;
    }
    return str.substring(2);
}

async function createSomeFilesAndDirectories(
    rootDir: string,
    filePrefix: string,
): Promise<FilesAndDirectories> {
    const step = async (dir: string, num: number, accDirs: string[], accFiles: string[]) => {
        // folders
        for (let i = 0; i < num; i++) {
            const subDir = path.join(dir, randomFileName());
            await fs.mkdir(subDir);
            accDirs.push(subDir);
            void step(subDir, num - 1, accDirs, accFiles);
        }

        // one file per level
        const file = path.join(dir, `${filePrefix}-${randomFileName()}.log`);
        accFiles.push(file);
    };
    const files: string[] = [];
    const directories: string[] = [];
    await step(rootDir, 3, directories, files);
    return {
        files,
        directories,
    };
}

async function exists(p: PathLike) {
    try {
        await fs.access(p, fsConstants.F_OK);
        return true;
    } catch {
        return false;
    }
}

async function deleteContents(dir: string): Promise<void> {
    for (const entry of await fs.readdir(dir, { withFileTypes: true })) {
        const x = path.join(dir, entry.name);
        if (entry.isDirectory()) {
            await deleteContents(x);
            await fs.rmdir(x);
        } else {
            await fs.unlink(x);
        }
    }
}

interface TestCase {
    name: string;
    fsSetup: (baseDir: string) => Promise<FilesAndDirectories>;
    logStep: (filesAndDirs: FilesAndDirectories) => Promise<string>;
}

function elementAtRandom<T>(xs: readonly T[]): T {
    return xs[Math.floor(Math.random() * xs.length)];
}

const testCases: TestCase[] = [
    {
        name: "simple",
        fsSetup: baseDir => createSomeFilesAndDirectories(baseDir, "simple"),
        logStep: async filesAndDirs => {
            const someFile = elementAtRandom(filesAndDirs.files);
            const lastAppended = randomString() + "\n";
            await fs.appendFile(someFile, lastAppended);
            return lastAppended;
        },
    },
    {
        name: "encoding",
        fsSetup: baseDir => createSomeFilesAndDirectories(baseDir, "encoding"),
        logStep: async filesAndDirs => {
            const someFile = elementAtRandom(filesAndDirs.files);
            const lastAppended = randomString() + "\n";
            const encoded = encode(lastAppended, "win1252");
            await fs.appendFile(someFile, encoded);
            return lastAppended;
        },
    },
    {
        name: "multipattern",
        fsSetup: async baseDir => {
            const aDir = path.join(baseDir, "a");
            await fs.mkdir(aDir);
            const bDir = path.join(baseDir, "b");
            await fs.mkdir(bDir);
            const aFilesAndDirs = await createSomeFilesAndDirectories(aDir, "multipattern");
            const bFilesAndDirs = await createSomeFilesAndDirectories(bDir, "multipattern");
            return {
                files: aFilesAndDirs.files.concat(bFilesAndDirs.files),
                directories: aFilesAndDirs.directories.concat(bFilesAndDirs.directories),
            };
        },
        logStep: async filesAndDirs => {
            const someFile = elementAtRandom(filesAndDirs.files);
            const lastAppended = randomString() + "\n";
            await fs.appendFile(someFile, lastAppended);
            return lastAppended;
        },
    },
];

interface TestCaseState {
    doc: vscode.TextDocument;
    filesAndDirs: FilesAndDirectories;
    expectedText: string;
}

suite("E2E", () => {
    test("Smoke test", async function () {
        let waitIntervalMs: number;
        if (process.env["CI"]) {
            waitIntervalMs = 1000;
            this.timeout(20000);
        } else {
            waitIntervalMs = 200;
            if (inspector.url()) {
                this.timeout(0);
            } else {
                this.timeout(5000);
            }
        }

        const workspaceRoot = vscode.workspace.workspaceFolders?.[0];
        assert(workspaceRoot);
        const logsBaseDir = path.resolve(workspaceRoot.uri.fsPath, "log");
        if (await exists(logsBaseDir)) {
            await deleteContents(logsBaseDir);
        } else {
            await fs.mkdir(logsBaseDir);
        }

        // makes logExplorer set __logViewer$logExplorerAndTreeView
        global.__logViewer$testing = true;

        const extension = vscode.extensions.getExtension("berublan.vscode-log-viewer");
        assert(extension);
        await extension.activate();

        const { logExplorer, treeView } = global.__logViewer$logExplorerAndTreeView;

        const textDocuments: vscode.TextDocument[] = [];
        const stateByTestCase: Record<string, TestCaseState> = {};

        for (const testCase of testCases) {
            const item = logExplorer
                .getChildren()
                ?.find(x => x.kind === "watch" && x.title === testCase.name);
            assert(item, `item not found for "${testCase.name}"`);
            await treeView.reveal(item, { select: true, expand: true });

            const command = logExplorer.getTreeItem(item).command;
            assert(command, `no command for "${testCase.name}"`);
            assert(command.arguments, `no command arguments for "${testCase.name}"`);

            // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
            await vscode.commands.executeCommand(command.command, ...command.arguments);
            const treeItem = logExplorer.getTreeItem(item);
            assert(
                treeItem.contextValue == "watching",
                `treeItem.contextValue for "${testCase.name}" not "watching"`,
            );

            const textEditor = vscode.window.activeTextEditor;
            assert(textEditor, `no text editor for "${testCase.name}"`);
            assert.strictEqual(textEditor.document.getText(), "no matching file found");

            textDocuments.push(textEditor.document);
            stateByTestCase[testCase.name] = {
                doc: textEditor.document,
                // will be updated below
                filesAndDirs: { files: [], directories: [] },
                expectedText: "",
            };
        }

        for (const testCase of testCases) {
            stateByTestCase[testCase.name].filesAndDirs = await testCase.fsSetup(logsBaseDir);
        }

        for (let i = 0; i < 10; i++) {
            // randomly change shown log
            await vscode.window.showTextDocument(elementAtRandom(textDocuments));
            for (const testCase of testCases) {
                const filesAndDirs = stateByTestCase[testCase.name].filesAndDirs;
                stateByTestCase[testCase.name].expectedText = await testCase.logStep(filesAndDirs);
            }
            await delay(waitIntervalMs);
            for (const testCase of testCases) {
                const { expectedText, doc } = stateByTestCase[testCase.name];
                const documentText = doc.getText();
                assert(
                    documentText.endsWith(expectedText),
                    `"${documentText}" didn't end with "${expectedText}" for "${testCase.name}"`,
                );
            }
        }

        await deleteContents(logsBaseDir);
        await delay(waitIntervalMs);

        for (const testCase of testCases) {
            assert.strictEqual(stateByTestCase[testCase.name].doc.getText(), "no matching file found");
        }

        // FIXME: is there any way to invoke context menu actions from here?
        await vscode.commands.executeCommand("logviewer.unwatchAll");
        const items = logExplorer.getChildren();
        assert(items);
        for (const item of items) {
            assert(item.kind === "watch");
            const treeItem = logExplorer.getTreeItem(item);
            assert(!treeItem.contextValue);
        }
    });
});
