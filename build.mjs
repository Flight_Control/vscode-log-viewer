import * as esbuild from "esbuild";
import * as path from "path";
import * as fs from "fs";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/** @type {"development"|"production"} */
const mode = process.env.NODE_ENV ?? "development";
const watch = Boolean(process.env.WATCH);

const outdir = path.join(__dirname, "dist");

fs.rmSync(outdir, { recursive: true, force: true });

/**
 * @param {import("esbuild").Metafile} meta
 * @param {boolean} watch
 */
function logMeta(meta, watch) {
    const yellowFG = "\u001b[33m";
    const grayFG = "\u001b[90m";
    const resetFG = "\u001b[39m";
    for (const [path, output] of Object.entries(meta.outputs)) {
        const sizeKB = (output.bytes / 1024).toFixed(1);
        let msg = "";
        if (output.entryPoint) {
            msg += `${grayFG}${output.entryPoint} ->${resetFG}`;
        }
        msg += ` ${path} ${yellowFG}${sizeKB}K${resetFG}`;
        console.log(msg);
    }
    if (watch) {
        // for problemMatcher, so that preLaunchTask works properly
        console.log("REBUILD");
        console.log("DONE");
    }
}

esbuild
    .build({
        bundle: true,
        target: ["node14"],
        platform: "node",
        outdir,
        define: {
            "process.env.NODE_ENV": JSON.stringify(mode),
            DEVELOPMENT: JSON.stringify(mode === "development"),
        },
        plugins: [
            {
                name: "split-iconv-lite",
                setup(build) {
                    build.onResolve({ filter: /^iconv-lite$/ }, args => {
                        if (args.kind === "entry-point") {
                            return;
                        }
                        return {
                            path: "./iconv-lite",
                            external: true,
                        };
                    });
                },
            },
        ],
        sourcemap: mode === "development" ? "inline" : undefined,
        metafile: true,
        watch: watch && {
            onRebuild: (_err, res) => {
                logMeta(res.metafile, watch);
            },
        },
        minify: mode === "production",
        entryPoints: {
            extension: path.join(__dirname, "src/extension.ts"),
            "iconv-lite": "iconv-lite",
        },
        external: ["vscode"],
    })
    .then(res => {
        logMeta(res.metafile, watch);
    })
    .catch(() => process.exit(1));
